-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
-- Please ANNOTATE WELL for a clear understanding of what the query is adding/changing/fixing.     --
--                           Overly detaling your query is NOT needed.                             --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
--        DO NOT RUN QUERIES ON THE LIVE SERVER BEFORE TESTING IN A ISOLATED DEV ENVIRONMENT       --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------


-- ----------
-- General --
-- ----------


-- Balga's Dais
INSERT IGNORE INTO mob_skills VALUES (1333,152,'contagion_transfer',0,7.0,2000,1500,4,0,0,0,0,0,0);
INSERT IGNORE INTO mob_skills VALUES (1334,154,'contamination',0,7.0,2000,1500,4,0,0,0,0,0,0);
INSERT IGNORE INTO mob_skills VALUES (1335,151,'toxic_pick',0,7.0,2000,1500,4,0,0,0,0,0,0);

-- Riverne Site #A01 & #B01
INSERT IGNORE INTO mob_skills VALUES (577,911,'jettatura',1,10.0,2000,1500,4,0,0,0,0,0,0); -- There's another Jettatura with AOE properties, but that should be ONLY for Nightmare_Hippogryph
INSERT IGNORE INTO mob_skills VALUES (580,914,'fantod',0,7.0,2000,1500,1,0,0,0,0,0,0); -- Hippogryph ability
INSERT IGNORE INTO mob_skills VALUES (579,913,'choke_breath',4,7.0,2000,1500,4,0,0,0,0,0,0); -- Hippogryph ability
UPDATE mob_skills SET mob_anim_id = 910 WHERE mob_skill_name = "hoof_volley"; -- Hippogryph ability - Wrong animation was set

-- Uleguerand Range
INSERT IGNORE INTO mob_skills VALUES (1475,847,'bull_rush_alt',0,7.0,500,500,4,16,0,3,0,0,0); -- Geush Urvan
INSERT IGNORE INTO mob_skills VALUES (1331,1075,'counterstance',0,7.0,2000,1500,4,0,0,0,0,0,0); -- Geush Urvan

-- ??
INSERT IGNORE INTO mob_skills VALUES (39,8,'spirits_within',0,7.0,2000,0,4,0,0,0,0,0,0); -- Spirits Within