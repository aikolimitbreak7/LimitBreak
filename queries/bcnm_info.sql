-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
-- Please ANNOTATE WELL for a clear understanding of what the query is adding/changing/fixing.     --
--                           Overly detaling your query is NOT needed.                             --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
--        DO NOT RUN QUERIES ON THE LIVE SERVER BEFORE TESTING IN A ISOLATED DEV ENVIRONMENT       --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------


-- -----------
-- GENERAL  --
-- -----------


UPDATE bcnm_info SET zoneId = "139", name = "rank_2_mission", timeLimit ="1800", levelcap = "25", partySize = "6", rules = "5", isMission = "1" WHERE bcnmID = 0; -- BCNM 0 is Rank 2-3 Dragon Fight - Horlaias Peak
UPDATE bcnm_info SET zoneId = "144", name = "rank_2_mission", timeLimit ="1800", levelcap = "25", partySize = "6", rules = "5", isMission = "1" WHERE bcnmID = 64; -- BCNM 64 is Rank 2-3 Dragon Fight - Waughroon Shrine
UPDATE bcnm_info SET zoneId = "146", name = "rank_2_mission", timeLimit ="1800", levelcap = "25", partySize = "6", rules = "5", isMission = "1" WHERE bcnmID = 96; -- BCNM 96 is Rank 2-3 Dragon Fight - Balgais Dais
UPDATE bcnm_info SET zoneId = "206", name = "rank_5_mission", timeLimit ="1800", levelcap = "50", partySize = "6", rules = "5", isMission = "1" WHERE bcnmID = 512; -- BCNM 512 is Rank 5-1 Fei'yin Skele Fight
UPDATE bcnm_info SET zoneId = "165", name = "shadow_lord_battle", timeLimit ="1800", levelcap = "75", partySize = "6", rules = "5", isMission = "1" WHERE bcnmID = 160; -- BCNM 160 is Rank 5-2 Shadow Lord Fight


-- Jade Sepulcher
UPDATE bcnm_info SET rules = "22" WHERE bcnmID = 1154; -- BCNM 1154 is BLU LB5

-- Bastok
UPDATE bcnm_info SET zoneId = "144", name = "on_my_way", timeLimit ="1800", levelcap = "75", partySize = "6", rules = "5", isMission = "1" WHERE bcnmID = 67; -- BCNM 67 is Rank 7-2 On My Way
UPDATE bcnm_info SET zoneId = "165", name = "where_two_paths_converge", timeLimit ="1800", levelcap = "75", partySize = "6", rules = "5", isMission = "1" WHERE bcnmID = 161; -- BCNM 161 is Rank 9-2 Where Two Paths Converge

-- San d'Oria
UPDATE bcnm_info SET zoneId = "140", name = "save_the_children", timeLimit ="1800", levelcap = "75", partySize = "6", rules = "5", isMission = "1" WHERE bcnmID = 32; -- BCNM 32 is Rank 1-3 Save the Children
UPDATE bcnm_info SET zoneId = "139", name = "the_secret_weapon", timeLimit ="1800", levelcap = "75", partySize = "6", rules = "5", isMission = "1" WHERE bcnmID = 3; -- BCNM 3 is Rank 7-2 The Secret Weapon
UPDATE bcnm_info SET zoneId = "206", name = "heir_to_the_light", timeLimit ="1800", levelcap = "75", partySize = "6", rules = "5", isMission = "1" WHERE bcnmID = 516; -- BCNM 516 is Rank 9-2 The Heir to the Light

-- Windurst
UPDATE bcnm_info SET zoneId = "146", name = "saintly_invitation", timeLimit ="1800", levelcap = "75", partySize = "6", rules = "5", isMission = "1" WHERE bcnmID = 99; -- BCNM 99 is Rank 6-2 Saintly Invitation
UPDATE bcnm_info SET zoneId = "170", name = "moon_reading", timeLimit ="1800", levelcap = "75", partySize = "6", rules = "5", isMission = "1" WHERE bcnmID = 225; -- BCNM 225 is Rank 9-2 Moon Reading

-- ---------------------
-- Rise of the Zilart --
-- ---------------------

-- ----------------------
-- Chains of Promathia --
-- ----------------------

-- --------------------------
-- Treasures of Aht Urghan --
-- --------------------------





-- Useful Queries

-- SELECT * FROM bcnm_info WHERE levelcap > 75; -- Shows list of all BCNM flagged for higher then 75 cap.  
-- UPDATE bcnm_info SET levelCap = "75" WHERE bcnmID IN (X,X,X) -- List all the bcnmIDs identified in above query to change all BCNM level caps that are above 75 to 75 instead.
