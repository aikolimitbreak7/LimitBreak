-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
-- Please ANNOTATE WELL for a clear understanding of what the query is adding/changing/fixing.     --
--                           Overly detaling your query is NOT needed.                             --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
--        DO NOT RUN QUERIES ON THE LIVE SERVER BEFORE TESTING IN A ISOLATED DEV ENVIRONMENT       --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------

-- -----------
-- GENERAL  --
-- -----------

-- CoP 5-3U Where Messengers Gather (Boneyard Gully Fight)
	-- Shikaree_X
UPDATE mob_pool_mods SET modid = "44" WHERE poolid = 3598; -- Sets mobid to dual_wield
UPDATE mob_pool_mods SET value = "1" WHERE poolid = 3598; -- dual_wield value to 1
UPDATE mob_pool_mods SET is_mob_mod = "1" WHERE poolid = 3598; -- If you want to use a modid from mob_modifer.h you need to set this to 1. Otherwise set to 0.
INSERT IGNORE INTO mob_pool_mods VALUE(3598,3,1,1); -- Add MP base to Shikaree_X

	-- Shikaree_Y
UPDATE mob_pool_mods SET modid = "291" WHERE poolid = 3600; -- Removed 70 innate regain and added it to .lua Added 15% counter rate as per wiki.
UPDATE mob_pool_mods SET value = "15" WHERE poolid = 3600; -- 15% counter rate	
	-- Shikaree_Z
DELETE from mob_pool_mods where poolid = 3601; -- Regain 70/tick was defined here. This was double dipping and is not needed for Shikaree_Z as regain is defined in the lua.

-- ---------
-- OTHERS --
-- ---------


