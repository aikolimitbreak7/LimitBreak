-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
-- Please ANNOTATE WELL for a clear understanding of what the query is adding/changing/fixing.     --
--                           Overly detaling your query is NOT needed.                             --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
--        DO NOT RUN QUERIES ON THE LIVE SERVER BEFORE TESTING IN A ISOLATED DEV ENVIRONMENT       --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------


-- ----------
-- GENERAL --
-- ----------

UPDATE synth_recipes SET HQCrystal = "4241" WHERE ID = "293" AND ResultName = "Adaman Chain" ; -- HQ crystal should be Terra and not Inferno
UPDATE synth_recipes SET HQCrystal = "4241" WHERE ID = "4143" AND ResultName = "Silver Belt" ; -- HQ crystal should be Terra and not Inferno
UPDATE synth_recipes SET HQCrystal = "4241" WHERE ID = "4144" AND ResultName = "Brass Fng. Gnt." ; -- HQ crystal should be Terra and not Inferno
UPDATE synth_recipes SET HQCrystal = "4241" WHERE ID = "4146" AND ResultName = "Tigereye Ring" ; -- HQ crystal should be Terra and not Inferno
UPDATE synth_recipes SET HQCrystal = "4241" WHERE ID = "4151" AND ResultName = "Peridot Earring" ; -- HQ crystal should be Terra and not Inferno


DELETE FROM synth_recipes WHERE Ingredient1 > 8804 AND Ingredient1 < 8917; -- Crafting kits are OOE
DELETE FROM synth_recipes WHERE Ingredient1 > 9483 AND Ingredient1 < 9524; -- Crafting kits are OOE


-- --------------
-- WOODWORKING --
-- --------------

UPDATE synth_recipes SET Cloth = "11" WHERE ID = "1" AND ResultName = "Simple Bed"; -- Seems to be Clothcraft 11 by comments here: https://ffxi.allakhazam.com/db/item.html?fitem=1926
UPDATE synth_recipes SET Gold = "57" WHERE ID = "38" AND ResultName = "Wardrobe"; -- Goldsmithing should be at least 57 as confirmed here: https://www.ffxiah.com/item/124/wardrobe
UPDATE synth_recipes SET Crystal = "4099" WHERE ID = "41" AND ResultName = "Coffee Table"; -- Confirmed that this is earth crystal: https://ffxi.allakhazam.com/db/item.html?fitem=10693. Sub levels aren't confirmed.
UPDATE synth_recipes SET ResultHQ1Qty = "1" WHERE ID = "405" AND ResultName = "Ancient Lumber"; -- Mythic Pole. No BG data. FFXiwiki says HQ1 is Ancient Lumberx1.
UPDATE synth_recipes SET Result = "930", ResultHQ1 = "851", ResultHQ2 = "717", ResultHQ3 = "809", ResultName = "Beastman Blood" WHERE ID = "498"; -- There is no agreement on desynth. Using FFXIWIKI data: https://ffxiclopedia.fandom.com/wiki/Frost_Shield
UPDATE synth_recipes SET ResultHQ2 = "710", ResultHQ3 = "710", ResultHQ2Qty = "1", ResultHQ3Qty = "2" WHERE ID = "825" and ResultName = "Coeurl Whisker"; -- Harp. As per BG and FFXIwiki HQ2/3 should be Chestnut lumberx1 and x2.
UPDATE synth_recipes SET ResultHQ2Qty = "10", ResultHQ3Qty = "12" WHERE ID = "1018" and ResultName = "Divine Sap"; -- HQ2 should yield 10 and HQ3 12.s
UPDATE synth_recipes SET Ingredient1 = "698" WHERE ID = "1062" and ResultName = "Bwtch. Ash Lbr."; -- Should use Ash Log and not Ash Lumber as confirmed here: https://www.ffxiah.com/item/1671/bwtch-ash-lbr
UPDATE synth_recipes SET Smith = "60" WHERE ID = "3221" AND ResultName = "Iron-splitter"; -- Most sites list this as Smithing 60.
UPDATE synth_recipes SET Smith = "60" WHERE ID = "3224" AND ResultName = "Steel-splitter"; -- Unconfirmed on the sub, but FFXIwiki has it at 60.
UPDATE synth_recipes SET ResultHQ3 = "718" WHERE ID = "3493" and ResultName = "Parchment"; -- Angel's Flute. Nerfing HQ3 following a patch from 06/09/08: https://www.bg-wiki.com/ffxi/Version_Update_(06/09/2008)
UPDATE synth_recipes SET KeyItem = "1987" WHERE ID = "3915" and ResultName = "Black Bolt"; -- This also requires keyitem "Boltmaker".
UPDATE synth_recipes SET Smith = "38" WHERE ID = "3343" AND ResultName = "Dark Mezraq"; -- Subcraft levels aren't confirmed anywhere, but this post confirms both subs should be Apprentice (38-50): https://www.bluegartr.com/threads/49240-Dark-mezraq-subcraft-question
DELETE FROM synth_recipes WHERE ID = "58" AND ResultName = "Totem Pole"; -- OOE. Added August 2007.
DELETE FROM synth_recipes WHERE ID = "86" AND ResultName = "Ryl. Sqr. Bunk"; -- OOE. Seems was added in 2008.
DELETE FROM synth_recipes WHERE ID = "95" AND ResultName = "Amiga Cactus"; -- OOE. Seems was added in 2008.
DELETE FROM synth_recipes WHERE ID = "98" AND ResultName = "Win. Tea Set"; -- OOE. Seems was added in 2008.
DELETE FROM synth_recipes WHERE ID = "99" AND ResultName = "Parclose"; -- OOE. Seems was added in 2008.
DELETE FROM synth_recipes WHERE ID = "1346" AND ResultName = "Teak Lumber"; -- OOE. Added in 2008.
DELETE FROM synth_recipes WHERE ID = "1347" AND ResultName = "Teak Lumber"; -- OOE. Added in 2008.
DELETE FROM synth_recipes WHERE ID = "3344" AND ResultName = "Broach Lance"; -- Added August 2007. Most likely WOTG data mining.
DELETE FROM synth_recipes WHERE ID = "3443" AND ResultName = "Obsidian Arrow"; -- OOE. Seems was added late 2007.
DELETE FROM synth_recipes WHERE ID = "3554" AND ResultName = "Butterfly Cage"; -- OOE. Seems was added in 2009.
DELETE FROM synth_recipes WHERE ID = "3730" AND ResultName = "Cricket Cage"; -- OOE. Seems was added in 2009.
DELETE FROM synth_recipes WHERE ID = "3731" AND ResultName = "Glowfly Cage"; -- OOE. Seems was added in 2009.
DELETE FROM synth_recipes WHERE ID = "3916" AND ResultName = "Valance"; -- OOE. Added in 2016.
DELETE FROM synth_recipes WHERE ID = "4458" AND ResultName = "Amigo Cactus"; -- OOE. Seems was added in 2008.
DELETE FROM synth_recipes WHERE ID = "4460" AND ResultName = "Stepping Stool"; -- OOE. Seems was added in 2009.
DELETE FROM synth_recipes WHERE ID = "4469" AND ResultName = "Fay Crozier"; -- OOE. Seems was added in 2009.
DELETE FROM synth_recipes WHERE ID = "4470" AND ResultName = "Qi Staff"; -- OOE. Seems was added in 2009.
DELETE FROM synth_recipes WHERE ID = "4475" AND ResultName = "Red Round Table"; -- OOE. Seems was added late 2008.
DELETE FROM synth_recipes WHERE ID = "4476" AND ResultName = "Blue Round Table"; -- OOE. Seems was added late 2008.
DELETE FROM synth_recipes WHERE ID = "4477" AND ResultName = "Green Rnd. Table"; -- OOE. Seems was added late 2008.
DELETE FROM synth_recipes WHERE ID = "4478" AND ResultName = "Yellow Rnd. Table"; -- OOE. Seems was added late 2008.
DELETE FROM synth_recipes WHERE ID = "4479" AND ResultName = "White Rnd. Table"; -- OOE. Seems was added late 2008.
DELETE FROM synth_recipes WHERE ID = "4481" AND ResultName = "Fay Gendawa"; -- OOE. Seems was added late 2009.
DELETE FROM synth_recipes WHERE ID = "4491" AND ResultName = "Bookstack"; -- OOE. Added in 2009.
DELETE FROM synth_recipes WHERE ID = "4492" AND ResultName = "Feasting Table"; -- OOE. Added in 2009.
DELETE FROM synth_recipes WHERE ID = "4493" AND ResultName = "Harp Stool"; -- OOE. Added in 2008.
DELETE FROM synth_recipes WHERE ID = "4494" AND ResultName = "Half Partition"; -- OOE. Added in 2009.
DELETE FROM synth_recipes WHERE ID = "45" AND ResultName = "Ngoma"; -- This was added August 2007 but probably through data mining of incoming WOTG content? For now will stay out unless we can confirm this is TOAU era.
DELETE FROM synth_recipes WHERE ID = "50" AND ResultName = "Cartonnier"; -- OOE. Added August 2007.
DELETE FROM synth_recipes WHERE ID = "81" AND ResultName = "Yellow Hobby Bo"; -- This was added August 2007 but probably through data mining of incoming WOTG content? For now will stay out unless we can confirm this is TOAU era.
DELETE FROM synth_recipes WHERE ID = "82" AND ResultName = "Red Hobby Bo"; -- This was added August 2007 but probably through data mining of incoming WOTG content? For now will stay out unless we can confirm this is TOAU era.
DELETE FROM synth_recipes WHERE ID = "83" AND ResultName = "Black Hobby Bo"; -- This was added August 2007 but probably through data mining of incoming WOTG content? For now will stay out unless we can confirm this is TOAU era.
DELETE FROM synth_recipes WHERE ID = "84" AND ResultName = "Blue Hobby Bo"; -- This was added August 2007 but probably through data mining of incoming WOTG content? For now will stay out unless we can confirm this is TOAU era.
DELETE FROM synth_recipes WHERE ID = "85" AND ResultName = "Green Hobby Bo"; -- This was added August 2007 but probably through data mining of incoming WOTG content? For now will stay out unless we can confirm this is TOAU era.
DELETE FROM synth_recipes WHERE ID = "89" AND ResultName = "Partition"; -- OOE. Seems was added in 2008.
DELETE FROM synth_recipes WHERE ID = "90" AND ResultName = "Credenza"; -- OOE. Seems was added in 2008.
DELETE FROM synth_recipes WHERE ID = "3345" AND ResultName = "Thalassocrat"; -- OOE. Added in 2008.
DELETE FROM synth_recipes WHERE ID = "3704" AND ResultName = "Dabo"; -- OOE. Added in 2008.
DELETE FROM synth_recipes WHERE ID = "3706" AND ResultName = "Jacaranda Lbr."; -- OOE. Added in 2008.
DELETE FROM synth_recipes WHERE ID = "3707" AND ResultName = "Jacaranda Lbr."; -- OOE. Added in 2008.
DELETE FROM synth_recipes WHERE ID = "3918" AND ResultName = "Trackers Bow"; -- OOE. Seems was added in 2009.
DELETE FROM synth_recipes WHERE ID = "3931" AND ResultName = "Feywld. Lumber"; -- OOE. Seems was added in 2009.
DELETE FROM synth_recipes WHERE ID = "4453" AND ResultName = "Passaddhi Staff"; -- OOE. Seems was added in 2009.
DELETE FROM synth_recipes WHERE ID = "4457" AND ResultName = "Tavern Bench"; -- OOE. Seems was added in 2009.
DELETE FROM synth_recipes WHERE ID = "4461" AND ResultName = "Red Viola Pot"; -- OOE. Seems was added in 2009.
DELETE FROM synth_recipes WHERE ID = "4462" AND ResultName = "Blue Viola Pot"; -- OOE. Seems was added in 2009.
DELETE FROM synth_recipes WHERE ID = "4463" AND ResultName = "Yellow Viola Pot"; -- OOE. Seems was added in 2009.
DELETE FROM synth_recipes WHERE ID = "4464" AND ResultName = "White Viola Pot"; -- OOE. Seems was added in 2009.
DELETE FROM synth_recipes WHERE ID = "4467" AND ResultName = "Fay Staff"; -- OOE. Seems was added in 2009.
DELETE FROM synth_recipes WHERE ID = "4473" AND ResultName = "Fay Lance"; -- OOE. Seems was added in 2009.
DELETE FROM synth_recipes WHERE ID = "4487" AND ResultName = "Spence"; -- OOE. Seems was added in 2009.
DELETE FROM synth_recipes WHERE ID = "4505" AND ResultName = "Winged Altar"; -- OOE. Added late 2007.