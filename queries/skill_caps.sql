-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
-- Please ANNOTATE WELL for a clear understanding of what the query is adding/changing/fixing.     --
--                           Overly detaling your query is NOT needed.                             --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
--        DO NOT RUN QUERIES ON THE LIVE SERVER BEFORE TESTING IN A ISOLATED DEV ENVIRONMENT       --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------


-- ------------------------------
-- Adjusting skill caps to ERA --
-- ------------------------------

-- Source: https://ffxiclopedia.fandom.com/wiki/Category:Combat_Skills?oldid=298667

-- values:
-- r1	A+
-- r2	A-	
-- r3	B+	
-- r4	B	
-- r5	B-	
-- r6	C+	
-- r7	C	
-- r8	C-	
-- r9	D	
-- r10	E	
-- r11	F	

UPDATE skill_caps SET r1 = 6, r2 = 6, r3 = 5, r4 = 5, r5 = 5, r6 = 5, r7 = 5, r8 = 5, r9 = 4, r10 = 4, r11 = 4 WHERE level = "1";
UPDATE skill_caps SET r1 = 9, r2 = 9, r3 = 7, r4 = 7, r5 = 7, r6 = 7, r7 = 7, r8 = 7, r9 = 6, r10 = 6, r11 = 6 WHERE level = "2";
UPDATE skill_caps SET r1 = 12, r2 = 12, r3 = 10, r4 = 10, r5 = 10, r6 = 10, r7 = 10, r8 = 10, r9 = 9, r10 = 9, r11 = 8 WHERE level = "3";
UPDATE skill_caps SET r1 = 15, r2 = 15, r3 = 13, r4 = 13, r5 = 13, r6 = 13, r7 = 13, r8 = 13, r9 = 12, r10 = 11, r11 = 10 WHERE level = "4";
UPDATE skill_caps SET r1 = 18, r2 = 18, r3 = 16, r4 = 16, r5 = 16, r6 = 16, r7 = 16, r8 = 16, r9 = 14, r10 = 14, r11 = 13 WHERE level = "5";
UPDATE skill_caps SET r1 = 21, r2 = 21, r3 = 19, r4 = 19, r5 = 19, r6 = 19, r7 = 19, r8 = 19, r9 = 17, r10 = 16, r11 = 15 WHERE level = "6";
UPDATE skill_caps SET r1 = 24, r2 = 24, r3 = 22, r4 = 22, r5 = 22, r6 = 21, r7 = 21, r8 = 21, r9 = 20, r10 = 19, r11 = 17 WHERE level = "7";
UPDATE skill_caps SET r1 = 27, r2 = 27, r3 = 25, r4 = 25, r5 = 25, r6 = 24, r7 = 24, r8 = 24, r9 = 22, r10 = 21, r11 = 20 WHERE level = "8";
UPDATE skill_caps SET r1 = 30, r2 = 30, r3 = 28, r4 = 28, r5 = 28, r6 = 27, r7 = 27, r8 = 27, r9 = 25, r10 = 24, r11 = 22 WHERE level = "9";
UPDATE skill_caps SET r1 = 33, r2 = 33, r3 = 31, r4 = 31, r5 = 31, r6 = 30, r7 = 30, r8 = 30, r9 = 28, r10 = 26, r11 = 24 WHERE level = "10";
UPDATE skill_caps SET r1 = 36, r2 = 36, r3 = 34, r4 = 34, r5 = 34, r6 = 33, r7 = 33, r8 = 33, r9 = 31, r10 = 29, r11 = 27 WHERE level = "11";
UPDATE skill_caps SET r1 = 39, r2 = 39, r3 = 36, r4 = 36, r5 = 36, r6 = 35, r7 = 35, r8 = 35, r9 = 33, r10 = 31, r11 = 29 WHERE level = "12";
UPDATE skill_caps SET r1 = 42, r2 = 42, r3 = 39, r4 = 39, r5 = 39, r6 = 38, r7 = 38, r8 = 38, r9 = 36, r10 = 34, r11 = 31 WHERE level = "13";
UPDATE skill_caps SET r1 = 45, r2 = 45, r3 = 42, r4 = 42, r5 = 42, r6 = 41, r7 = 41, r8 = 41, r9 = 39, r10 = 36, r11 = 33 WHERE level = "14";
UPDATE skill_caps SET r1 = 48, r2 = 48, r3 = 45, r4 = 45, r5 = 45, r6 = 44, r7 = 44, r8 = 44, r9 = 41, r10 = 39, r11 = 36 WHERE level = "15";
UPDATE skill_caps SET r1 = 51, r2 = 51, r3 = 48, r4 = 48, r5 = 48, r6 = 47, r7 = 47, r8 = 47, r9 = 44, r10 = 41, r11 = 38 WHERE level = "16";
UPDATE skill_caps SET r1 = 54, r2 = 54, r3 = 51, r4 = 51, r5 = 51, r6 = 49, r7 = 49, r8 = 49, r9 = 47, r10 = 44, r11 = 40 WHERE level = "17";
UPDATE skill_caps SET r1 = 57, r2 = 57, r3 = 54, r4 = 54, r5 = 54, r6 = 52, r7 = 52, r8 = 52, r9 = 49, r10 = 46, r11 = 43 WHERE level = "18";
UPDATE skill_caps SET r1 = 60, r2 = 60, r3 = 57, r4 = 57, r5 = 57, r6 = 55, r7 = 55, r8 = 55, r9 = 52, r10 = 49, r11 = 45 WHERE level = "19";
UPDATE skill_caps SET r1 = 63, r2 = 63, r3 = 60, r4 = 60, r5 = 60, r6 = 58, r7 = 58, r8 = 58, r9 = 55, r10 = 51, r11 = 47 WHERE level = "20";
UPDATE skill_caps SET r1 = 66, r2 = 66, r3 = 63, r4 = 63, r5 = 63, r6 = 61, r7 = 61, r8 = 61, r9 = 58, r10 = 54, r11 = 50 WHERE level = "21";
UPDATE skill_caps SET r1 = 69, r2 = 69, r3 = 65, r4 = 65, r5 = 65, r6 = 63, r7 = 63, r8 = 63, r9 = 60, r10 = 56, r11 = 52 WHERE level = "22";
UPDATE skill_caps SET r1 = 72, r2 = 72, r3 = 68, r4 = 68, r5 = 68, r6 = 66, r7 = 66, r8 = 66, r9 = 63, r10 = 59, r11 = 54 WHERE level = "23";
UPDATE skill_caps SET r1 = 75, r2 = 75, r3 = 71, r4 = 71, r5 = 71, r6 = 69, r7 = 69, r8 = 69, r9 = 66, r10 = 61, r11 = 56 WHERE level = "24";
UPDATE skill_caps SET r1 = 78, r2 = 78, r3 = 74, r4 = 74, r5 = 74, r6 = 72, r7 = 72, r8 = 72, r9 = 68, r10 = 64, r11 = 59 WHERE level = "25";
UPDATE skill_caps SET r1 = 81, r2 = 81, r3 = 77, r4 = 77, r5 = 77, r6 = 75, r7 = 75, r8 = 75, r9 = 71, r10 = 66, r11 = 61 WHERE level = "26";
UPDATE skill_caps SET r1 = 84, r2 = 84, r3 = 80, r4 = 80, r5 = 80, r6 = 77, r7 = 77, r8 = 77, r9 = 74, r10 = 69, r11 = 63 WHERE level = "27";
UPDATE skill_caps SET r1 = 87, r2 = 87, r3 = 83, r4 = 83, r5 = 83, r6 = 80, r7 = 80, r8 = 80, r9 = 76, r10 = 71, r11 = 66 WHERE level = "28";
UPDATE skill_caps SET r1 = 90, r2 = 90, r3 = 86, r4 = 86, r5 = 86, r6 = 83, r7 = 83, r8 = 83, r9 = 79, r10 = 74, r11 = 68 WHERE level = "29";
UPDATE skill_caps SET r1 = 93, r2 = 93, r3 = 89, r4 = 89, r5 = 89, r6 = 86, r7 = 86, r8 = 86, r9 = 82, r10 = 76, r11 = 70 WHERE level = "30";
UPDATE skill_caps SET r1 = 96, r2 = 96, r3 = 92, r4 = 92, r5 = 92, r6 = 89, r7 = 89, r8 = 89, r9 = 85, r10 = 79, r11 = 73 WHERE level = "31";
UPDATE skill_caps SET r1 = 99, r2 = 99, r3 = 94, r4 = 94, r5 = 94, r6 = 91, r7 = 91, r8 = 91, r9 = 87, r10 = 81, r11 = 75 WHERE level = "32";
UPDATE skill_caps SET r1 = 102, r2 = 102, r3 = 97, r4 = 97, r5 = 97, r6 = 94, r7 = 94, r8 = 94, r9 = 90, r10 = 84, r11 = 77 WHERE level = "33";
UPDATE skill_caps SET r1 = 105, r2 = 105, r3 = 100, r4 = 100, r5 = 100, r6 = 97, r7 = 97, r8 = 97, r9 = 93, r10 = 86, r11 = 79 WHERE level = "34";
UPDATE skill_caps SET r1 = 108, r2 = 108, r3 = 103, r4 = 103, r5 = 103, r6 = 100, r7 = 100, r8 = 100, r9 = 95, r10 = 89, r11 = 82 WHERE level = "35";
UPDATE skill_caps SET r1 = 111, r2 = 111, r3 = 106, r4 = 106, r5 = 106, r6 = 103, r7 = 103, r8 = 103, r9 = 98, r10 = 91, r11 = 84 WHERE level = "36";
UPDATE skill_caps SET r1 = 114, r2 = 114, r3 = 109, r4 = 109, r5 = 109, r6 = 105, r7 = 105, r8 = 105, r9 = 101, r10 = 94, r11 = 86 WHERE level = "37";
UPDATE skill_caps SET r1 = 117, r2 = 117, r3 = 112, r4 = 112, r5 = 112, r6 = 108, r7 = 108, r8 = 108, r9 = 103, r10 = 96, r11 = 89 WHERE level = "38";
UPDATE skill_caps SET r1 = 120, r2 = 120, r3 = 115, r4 = 115, r5 = 115, r6 = 111, r7 = 111, r8 = 111, r9 = 106, r10 = 99, r11 = 91 WHERE level = "39";
UPDATE skill_caps SET r1 = 123, r2 = 123, r3 = 118, r4 = 118, r5 = 118, r6 = 114, r7 = 114, r8 = 114, r9 = 109, r10 = 101, r11 = 93 WHERE level = "40";
UPDATE skill_caps SET r1 = 126, r2 = 126, r3 = 121, r4 = 121, r5 = 121, r6 = 117, r7 = 117, r8 = 117, r9 = 112, r10 = 104, r11 = 96 WHERE level = "41";
UPDATE skill_caps SET r1 = 129, r2 = 129, r3 = 123, r4 = 123, r5 = 123, r6 = 119, r7 = 119, r8 = 119, r9 = 114, r10 = 106, r11 = 98 WHERE level = "42";
UPDATE skill_caps SET r1 = 132, r2 = 132, r3 = 126, r4 = 126, r5 = 126, r6 = 122, r7 = 122, r8 = 122, r9 = 117, r10 = 109, r11 = 100 WHERE level = "43";
UPDATE skill_caps SET r1 = 135, r2 = 135, r3 = 129, r4 = 129, r5 = 129, r6 = 125, r7 = 125, r8 = 125, r9 = 120, r10 = 111, r11 = 102 WHERE level = "44";
UPDATE skill_caps SET r1 = 138, r2 = 138, r3 = 132, r4 = 132, r5 = 132, r6 = 128, r7 = 128, r8 = 128, r9 = 122, r10 = 114, r11 = 105 WHERE level = "45";
UPDATE skill_caps SET r1 = 141, r2 = 141, r3 = 135, r4 = 135, r5 = 135, r6 = 131, r7 = 131, r8 = 131, r9 = 125, r10 = 116, r11 = 107 WHERE level = "46";
UPDATE skill_caps SET r1 = 144, r2 = 144, r3 = 138, r4 = 138, r5 = 138, r6 = 133, r7 = 133, r8 = 133, r9 = 128, r10 = 119, r11 = 109 WHERE level = "47";
UPDATE skill_caps SET r1 = 147, r2 = 147, r3 = 141, r4 = 141, r5 = 141, r6 = 136, r7 = 136, r8 = 136, r9 = 130, r10 = 121, r11 = 112 WHERE level = "48";
UPDATE skill_caps SET r1 = 150, r2 = 150, r3 = 144, r4 = 144, r5 = 144, r6 = 139, r7 = 139, r8 = 139, r9 = 133, r10 = 124, r11 = 114 WHERE level = "49";
UPDATE skill_caps SET r1 = 153, r2 = 153, r3 = 147, r4 = 147, r5 = 147, r6 = 142, r7 = 142, r8 = 142, r9 = 136, r10 = 126, r11 = 116 WHERE level = "50";
UPDATE skill_caps SET r1 = 158, r2 = 158, r3 = 151, r4 = 151, r5 = 151, r6 = 146, r7 = 146, r8 = 146, r9 = 140, r10 = 130, r11 = 120 WHERE level = "51";
UPDATE skill_caps SET r1 = 163, r2 = 163, r3 = 156, r4 = 156, r5 = 156, r6 = 151, r7 = 151, r8 = 151, r9 = 145, r10 = 135, r11 = 124 WHERE level = "52";
UPDATE skill_caps SET r1 = 168, r2 = 168, r3 = 161, r4 = 161, r5 = 161, r6 = 156, r7 = 156, r8 = 156, r9 = 150, r10 = 139, r11 = 128 WHERE level = "53";
UPDATE skill_caps SET r1 = 173, r2 = 173, r3 = 166, r4 = 166, r5 = 166, r6 = 161, r7 = 161, r8 = 161, r9 = 154, r10 = 144, r11 = 133 WHERE level = "54";
UPDATE skill_caps SET r1 = 178, r2 = 178, r3 = 171, r4 = 171, r5 = 171, r6 = 166, r7 = 166, r8 = 166, r9 = 159, r10 = 148, r11 = 137 WHERE level = "55";
UPDATE skill_caps SET r1 = 183, r2 = 183, r3 = 176, r4 = 176, r5 = 176, r6 = 170, r7 = 170, r8 = 170, r9 = 164, r10 = 153, r11 = 141 WHERE level = "56";
UPDATE skill_caps SET r1 = 188, r2 = 188, r3 = 181, r4 = 181, r5 = 181, r6 = 175, r7 = 175, r8 = 175, r9 = 168, r10 = 157, r11 = 146 WHERE level = "57";
UPDATE skill_caps SET r1 = 193, r2 = 193, r3 = 186, r4 = 186, r5 = 186, r6 = 180, r7 = 180, r8 = 180, r9 = 173, r10 = 162, r11 = 150 WHERE level = "58";
UPDATE skill_caps SET r1 = 198, r2 = 198, r3 = 191, r4 = 191, r5 = 191, r6 = 185, r7 = 185, r8 = 185, r9 = 178, r10 = 166, r11 = 154 WHERE level = "59";
UPDATE skill_caps SET r1 = 203, r2 = 203, r3 = 196, r4 = 196, r5 = 196, r6 = 190, r7 = 190, r8 = 190, r9 = 183, r10 = 171, r11 = 159 WHERE level = "60";
UPDATE skill_caps SET r1 = 207, r2 = 207, r3 = 199, r4 = 199, r5 = 198, r6 = 192, r7 = 192, r8 = 192, r9 = 184, r10 = 172, r11 = 161 WHERE level = "61";
UPDATE skill_caps SET r1 = 212, r2 = 211, r3 = 203, r4 = 202, r5 = 201, r6 = 195, r7 = 194, r8 = 194, r9 = 186, r10 = 174, r11 = 163 WHERE level = "62";
UPDATE skill_caps SET r1 = 217, r2 = 215, r3 = 207, r4 = 205, r5 = 204, r6 = 197, r7 = 196, r8 = 196, r9 = 188, r10 = 176, r11 = 165 WHERE level = "63";
UPDATE skill_caps SET r1 = 222, r2 = 219, r3 = 210, r4 = 208, r5 = 206, r6 = 200, r7 = 199, r8 = 198, r9 = 190, r10 = 178, r11 = 167 WHERE level = "64";
UPDATE skill_caps SET r1 = 227, r2 = 223, r3 = 214, r4 = 212, r5 = 209, r6 = 202, r7 = 201, r8 = 200, r9 = 192, r10 = 180, r11 = 169 WHERE level = "65";
UPDATE skill_caps SET r1 = 232, r2 = 227, r3 = 218, r4 = 215, r5 = 212, r6 = 205, r7 = 203, r8 = 202, r9 = 194, r10 = 182, r11 = 171 WHERE level = "66";
UPDATE skill_caps SET r1 = 236, r2 = 231, r3 = 221, r4 = 218, r5 = 214, r6 = 207, r7 = 205, r8 = 204, r9 = 195, r10 = 184, r11 = 173 WHERE level = "67";
UPDATE skill_caps SET r1 = 241, r2 = 235, r3 = 225, r4 = 221, r5 = 217, r6 = 210, r7 = 208, r8 = 206, r9 = 197, r10 = 186, r11 = 175 WHERE level = "68";
UPDATE skill_caps SET r1 = 246, r2 = 239, r3 = 229, r4 = 225, r5 = 220, r6 = 212, r7 = 210, r8 = 208, r9 = 199, r10 = 188, r11 = 177 WHERE level = "69";
UPDATE skill_caps SET r1 = 251, r2 = 244, r3 = 233, r4 = 228, r5 = 223, r6 = 215, r7 = 212, r8 = 210, r9 = 201, r10 = 190, r11 = 179 WHERE level = "70";
UPDATE skill_caps SET r1 = 256, r2 = 249, r3 = 237, r4 = 232, r5 = 226, r6 = 218, r7 = 214, r8 = 212, r9 = 203, r10 = 192, r11 = 181 WHERE level = "71";
UPDATE skill_caps SET r1 = 261, r2 = 254, r3 = 241, r4 = 236, r5 = 229, r6 = 221, r7 = 217, r8 = 214, r9 = 205, r10 = 194, r11 = 183 WHERE level = "72";
UPDATE skill_caps SET r1 = 266, r2 = 259, r3 = 246, r4 = 240, r5 = 232, r6 = 224, r7 = 219, r8 = 216, r9 = 207, r10 = 196, r11 = 185 WHERE level = "73";
UPDATE skill_caps SET r1 = 271, r2 = 264, r3 = 251, r4 = 245, r5 = 236, r6 = 227, r7 = 222, r8 = 218, r9 = 208, r10 = 198, r11 = 187 WHERE level = "74";
UPDATE skill_caps SET r1 = 276, r2 = 269, r3 = 256, r4 = 250, r5 = 240, r6 = 230, r7 = 225, r8 = 220, r9 = 210, r10 = 200, r11 = 189 WHERE level = "75";