-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
-- Please ANNOTATE WELL for a clear understanding of what the query is adding/changing/fixing.     --
--                           Overly detaling your query is NOT needed.                             --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------
--        DO NOT RUN QUERIES ON THE LIVE SERVER BEFORE TESTING IN A ISOLATED DEV ENVIRONMENT       --
-- --------------------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------------------------


-- ----------
-- General --
-- ----------

-- Jade Sepulcher
INSERT IGNORE INTO mob_spell_lists VALUES ('Raubahn',213,530,1,255); -- Refueling - BLU LB5
INSERT IGNORE INTO mob_spell_lists VALUES ('Raubahn',213,593,1,255); -- Magic Fruit - BLU LB5
INSERT IGNORE INTO mob_spell_lists VALUES ('Raubahn',213,595,1,255); -- 1000 Needles - BLU LB5
INSERT IGNORE INTO mob_spell_lists VALUES ('Raubahn',213,538,1,255); -- Memento Mori - BLU LB5
INSERT IGNORE INTO mob_spell_lists VALUES ('Raubahn',213,594,1,255); -- Uppercut - BLU LB5
INSERT IGNORE INTO mob_spell_lists VALUES ('Raubahn',213,641,1,255); -- Hysteric Barage - BLU LB5
INSERT IGNORE INTO mob_spell_lists VALUES ('Raubahn',213,632,1,255); -- Diamondhide - BLU LB5
INSERT IGNORE INTO mob_spell_lists VALUES ('Raubahn',213,642,1,255); -- Amplification - BLU LB5
INSERT IGNORE INTO mob_spell_lists VALUES ('Raubahn',213,547,1,255); -- Cocoon - BLU LB5
INSERT IGNORE INTO mob_spell_lists VALUES ('Raubahn',213,548,1,255); -- Filamented Hold - BLU LB5
INSERT IGNORE INTO mob_spell_lists VALUES ('Raubahn',213,633,1,255); -- Enervation - BLU LB5
INSERT IGNORE INTO mob_spell_lists VALUES ('Raubahn',213,540,1,255); -- Spinal Cleave - BLU LB5
INSERT IGNORE INTO mob_spell_lists VALUES ('Raubahn',213,608,1,255); -- Frost Breath - BLU LB5
INSERT IGNORE INTO mob_spell_lists VALUES ('Raubahn',213,640,1,255); -- Tail Slap - BLU LB5
INSERT IGNORE INTO mob_spell_lists VALUES ('Raubahn',213,574,1,255); -- Feather Barrier - BLU LB5
INSERT IGNORE INTO mob_spell_lists VALUES ('Raubahn',213,604,1,255); -- Bad Breath - BLU LB5

-- Uleguerand Range
INSERT IGNORE INTO mob_spell_lists VALUES ('Geush_Urvan',436,152,1,255); -- Blizzard IV
INSERT IGNORE INTO mob_spell_lists VALUES ('Geush_Urvan',436,181,1,255); -- Blizzaga III
INSERT IGNORE INTO mob_spell_lists VALUES ('Geush_Urvan',436,356,1,255); -- Paralyga
INSERT IGNORE INTO mob_spell_lists VALUES ('Geush_Urvan',436,362,1,255); -- Bindga

-- CoP 5-3U Where Messengers Gather (Boneyard Gully Fight)
	-- Shikaree_X Added all /nin spells up to BST50/NIN25
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_X',451,338,1,255); -- Utsusemi: Ichi
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_X',451,320,1,255); -- Katon_ichi
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_X',451,323,1,255); -- hyoton_ichi
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_X',451,326,1,255); -- huton_ichi
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_X',451,329,1,255); -- doton_ichi
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_X',451,332,1,255); -- raiton_ichi
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_X',451,335,1,255); -- suiton_ichi
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_X',451,347,1,255); -- kurayami_ichi
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_X',451,344,1,255); -- hojo_ichi
	-- Shikaree_Y Added most powerful DRK spells for level 50
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Y',452,144,1,255); -- Fire
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Y',452,149,1,255); -- Blizzard
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Y',452,154,1,255); -- Aero
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Y',452,160,1,255); -- Stone II	
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Y',452,164,1,255); -- Thunder	
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Y',452,170,1,255); -- Water II
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Y',452,220,1,255); -- Poison **REMOVE POISON 2 (221)**
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Y',452,231,1,255); -- Bio II
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Y',452,245,1,255); -- Drain
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Y',452,247,1,255); -- Aspir
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Y',452,252,1,255); -- Stun
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Y',452,266,1,255); -- Absorb-STR
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Y',452,267,1,255); -- Absorb-DEX
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Y',452,268,1,255); -- Absorb-VIT
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Y',452,269,1,255); -- Absorb-AGI
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Y',452,270,1,255); -- Absorb-INT
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Y',452,271,1,255); -- Absorb-MND
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Y',452,272,1,255); -- Absorb-CHR
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Y',452,275,1,255); -- Absorb-TP
	-- Shikaree_Z Added all /whm spells up to DRG50/WHM25
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Z',450,3,1,255); -- Cure III	
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Z',450,15,1,255); -- Paralyna
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Z',450,17,1,255); -- Silena
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Z',450,14,1,255); -- Poisona
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Z',450,16,1,255); -- Blindna
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Z',450,23,1,255); -- Dia
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Z',450,28,1,255); -- Banish
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Z',450,33,1,255); -- Diaga
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Z',450,38,1,255); -- Banishga
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Z',450,43,1,255); -- Protect
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Z',450,48,1,255); -- Shell
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Z',450,53,1,255); -- Blink
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Z',450,55,1,255); -- Aquaveil
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Z',450,56,1,255); -- Slow
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Z',450,58,1,255); -- Paralyze
INSERT IGNORE INTO mob_spell_lists VALUES ('Shikaree_Z',450,59,1,255); -- Silence

-- CoP 5-3L Past Sins (Mineshaft 2716 Fight)
	-- Chekochuk Spell List (Used three different FFXI Captures) https://youtu.be/lM65yVpKVGw   https://youtu.be/KM6KAX2iAQ0   https://youtu.be/uhLEnV-3VXs
INSERT IGNORE INTO mob_spell_lists VALUES ('Chekochuk',453,146,1,255); -- Fire III
INSERT IGNORE INTO mob_spell_lists VALUES ('Chekochuk',453,156,1,255); -- Aero III
INSERT IGNORE INTO mob_spell_lists VALUES ('Chekochuk',453,171,1,255); -- Water III
INSERT IGNORE INTO mob_spell_lists VALUES ('Chekochuk',453,195,1,255); -- Thundaga II
INSERT IGNORE INTO mob_spell_lists VALUES ('Chekochuk',453,204,1,255); -- Flare
INSERT IGNORE INTO mob_spell_lists VALUES ('Chekochuk',453,210,1,255); -- Quake
INSERT IGNORE INTO mob_spell_lists VALUES ('Chekochuk',453,212,1,255); -- Burst
INSERT IGNORE INTO mob_spell_lists VALUES ('Chekochuk',453,214,1,255); -- Flood
INSERT IGNORE INTO mob_spell_lists VALUES ('Chekochuk',453,231,1,255); -- Bio II
INSERT IGNORE INTO mob_spell_lists VALUES ('Chekochuk',453,235,1,255); -- Burn
INSERT IGNORE INTO mob_spell_lists VALUES ('Chekochuk',453,236,1,255); -- Frost
INSERT IGNORE INTO mob_spell_lists VALUES ('Chekochuk',453,237,1,255); -- Choke
INSERT IGNORE INTO mob_spell_lists VALUES ('Chekochuk',453,240,1,255); -- Drown
INSERT IGNORE INTO mob_spell_lists VALUES ('Chekochuk',453,245,1,255); -- Drain
INSERT IGNORE INTO mob_spell_lists VALUES ('Chekochuk',453,247,1,255); -- Aspir
INSERT IGNORE INTO mob_spell_lists VALUES ('Chekochuk',453,249,1,255); -- Blaze Spikes
INSERT IGNORE INTO mob_spell_lists VALUES ('Chekochuk',453,252,1,255); -- Stun
INSERT IGNORE INTO mob_spell_lists VALUES ('Chekochuk',453,253,1,255); -- Sleep
INSERT IGNORE INTO mob_spell_lists VALUES ('Chekochuk',453,254,1,255); -- Blind
INSERT IGNORE INTO mob_spell_lists VALUES ('Chekochuk',453,258,1,255); -- Bind
INSERT IGNORE INTO mob_spell_lists VALUES ('Chekochuk',453,259,1,255); -- Sleep II
INSERT IGNORE INTO mob_spell_lists VALUES ('Chekochuk',453,273,1,255); -- Sleepga
INSERT IGNORE INTO mob_spell_lists VALUES ('Chekochuk',453,274,1,255); -- Sleepga II
	-- Movamuq Spell List (Used three different FFXI Captures) https://youtu.be/lM65yVpKVGw   https://youtu.be/KM6KAX2iAQ0   https://youtu.be/uhLEnV-3VXs
INSERT IGNORE INTO mob_spell_lists VALUES ('Movamuq',454,5,1,255); -- Cure V
INSERT IGNORE INTO mob_spell_lists VALUES ('Movamuq',454,14,1,255); -- Poisona
INSERT IGNORE INTO mob_spell_lists VALUES ('Movamuq',454,15,1,255); -- Paralyna
INSERT IGNORE INTO mob_spell_lists VALUES ('Movamuq',454,21,1,255); -- Holy
INSERT IGNORE INTO mob_spell_lists VALUES ('Movamuq',454,24,1,255); -- Dia II
INSERT IGNORE INTO mob_spell_lists VALUES ('Movamuq',454,29,1,255); -- Banish II
INSERT IGNORE INTO mob_spell_lists VALUES ('Movamuq',454,34,1,255); -- Diaga II
INSERT IGNORE INTO mob_spell_lists VALUES ('Movamuq',454,39,1,255); -- Banishga II
INSERT IGNORE INTO mob_spell_lists VALUES ('Movamuq',454,45,1,255); -- Protect III
INSERT IGNORE INTO mob_spell_lists VALUES ('Movamuq',454,46,1,255); -- Protect IV
INSERT IGNORE INTO mob_spell_lists VALUES ('Movamuq',454,50,1,255); -- Shell III
INSERT IGNORE INTO mob_spell_lists VALUES ('Movamuq',454,53,1,255); -- Blink
INSERT IGNORE INTO mob_spell_lists VALUES ('Movamuq',454,54,1,255); -- Stoneskin
INSERT IGNORE INTO mob_spell_lists VALUES ('Movamuq',454,55,1,255); -- Aquaveil
INSERT IGNORE INTO mob_spell_lists VALUES ('Movamuq',454,56,1,255); -- Slow
INSERT IGNORE INTO mob_spell_lists VALUES ('Movamuq',454,57,1,255); -- Haste
INSERT IGNORE INTO mob_spell_lists VALUES ('Movamuq',454,58,1,255); -- Paralyze
INSERT IGNORE INTO mob_spell_lists VALUES ('Movamuq',454,59,1,255); -- Silence
INSERT IGNORE INTO mob_spell_lists VALUES ('Movamuq',454,112,1,255); -- Flash
	-- Trikotrak Spell List (Used three different FFXI Captures) https://youtu.be/lM65yVpKVGw   https://youtu.be/KM6KAX2iAQ0   https://youtu.be/uhLEnV-3VXs
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',455,4,1,255); -- Cure IV
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',455,24,1,255); -- Dia II
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',455,34,1,255); -- Diaga II
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',455,50,1,255); -- Shell III
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',455,53,1,255); -- Blink
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',455,54,1,255); -- Stoneskin
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',455,55,1,255); -- Aquaveil
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',455,56,1,255); -- Slow
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',455,57,1,255); -- Haste
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',455,105,1,255); -- Enwater
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',455,108,1,255); -- Regen
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',455,145,1,255); -- Fire II
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',455,150,1,255); -- Blizzard II
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',455,155,1,255); -- Aero II
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',455,160,1,255); -- Stone II
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',455,165,1,255); -- Thunder II
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',455,170,1,255); -- Water II
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',455,216,1,255); -- Gravity
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',455,221,1,255); -- Poison II
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',455,231,1,255); -- Bio II
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',455,253,1,255); -- Sleep
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',455,254,1,255); -- Blind
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',455,258,1,255); -- Bind
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',455,259,1,255); -- Sleep II
INSERT IGNORE INTO mob_spell_lists VALUES ('Trikotrak',455,260,1,255); -- Dispel