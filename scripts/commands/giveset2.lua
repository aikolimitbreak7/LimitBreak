---------------------------------------------------------------------------------------------------
-- func: giveset2 <player> <set name>
-- desc: Gives a set to the target player.
-- courtesy of coreyms/topaz
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 3,
    parameters = "ss"
}

function onTrigger(player, target, setName)

	if(target == nil) then
		player:PrintToPlayer("You must enter a valid player name and set name.")
		player:PrintToPlayer("!giveset2 <player> <set name>")
		player:PrintToPlayer("For list of items, do: !giveset2 list")
		return
	end

    if (setName == nil) then
		if (target == "list") then
			player:PrintToPlayer("war35, test,")
		--	player:PrintToPlayer("shadowmelee, valkyriemelee, crimson, blood, enkidu, oracle, aurum, homam, nashira, hachiryu, askar, skygod, nocturnus,")
		--	player:PrintToPlayer("ares, skadi, usukane, marduk, morrigan, amir, pahluwan, yigit, valhalla, mahatma, goliard, blessed, blessed1, dusk, dusk1")
		else
			player:PrintToPlayer("You must enter a valid player name and set name.")
			player:PrintToPlayer("!giveset2 <player> <set name>")
			player:PrintToPlayer("For list of items, do: !giveset2 list")
        end
		return
    end

    local targ = GetPlayerByName( target )
    if (targ == nil) then
        player:PrintToPlayer( string.format( "Player named '%s' not found!", target ) )
        return
    end

	local setGiven = true;

    -- Attempt to give the target the item..
    if (targ:getFreeSlotsCount() < 15) then
        player:PrintToPlayer( string.format( "Player '%s' does not have free space for those items!", target ) )
    else
		if (setName == "war35") then
			 targ:addItem(12486) -- Emperor Hairpin
			 targ:addItem(12566) -- Centurion Scale Mail
			 targ:addItem(12761) -- Custom M Gloves
			 targ:addItem(13014) -- Leaping Boots
			 targ:addItem(13015) -- Custom M Boots
			 targ:addItem(13061) -- Spike Necklace
			 targ:addItem(13225) -- Brave Belt
			 targ:addItem(13326) -- Beetle Earring +1
			 targ:addItem(13326) -- Beetle Earring +1
			 targ:addItem(13522) -- Courage Ring
			 targ:addItem(13522) -- Courage Ring
			 targ:addItem(13631) -- Nomad's Mantle
			 targ:addItem(14260) -- Republic Subligar
			 targ:addItem(16706) -- Heavy Axe
			 targ:addItem(19009) -- Brass Grip
			 
		elseif (setName == "test") then
			 targ:addItem(16119)
		else
			setGiven = false;
		end
		if(setGiven) then
			player:PrintToPlayer( string.format( "Gave player '%s' the set '%s' ", target, setName ) )
		else
			player:PrintToPlayer("You must specify a valid set name, !giveset2 list")
		end
    end
end