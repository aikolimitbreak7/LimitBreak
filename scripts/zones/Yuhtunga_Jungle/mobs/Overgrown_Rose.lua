-----------------------------------
-- Area: Yuhtunga Jungle
--  Mob: Overgrown Rose
-----------------------------------
local ID = require("scripts/zones/Yuhtunga_Jungle/IDs")
require("scripts/globals/status")
-----------------------------------

function onMobSpawn(mob)
    local rosere = GetServerVariable("RosetimeToGrow")
    local roseup = GetServerVariable("RoseGardenUP")
    local vivianup = GetServerVariable("VivianUP")
    if mob:getID() == ID.mob.ROSE_GARDEN_PH then
        if rosere > os.time() then
            return -- If server restart/crashes, keep first roll
        elseif rosere < os.time() and roseup == 0 and vivianup == 0 then 
            SetServerVariable("RosetimeToGrow", os.time() + math.random(60, 61)) -- 10:00:00 to 10:30:00
        end
    end
end

function onMobDisengage(mob)
    if mob:getID() == ID.mob.ROSE_GARDEN_PH then
        SetServerVariable("RosetimeToGrow", os.time() + math.random(60, 61)) -- 10:00:00 to 10:30:00
    end
end

function onMobRoam(mob)
    -- Rose Garden PH has been left alone for 10.25 hours
    local rosere = GetServerVariable("RosetimeToGrow")
    if mob:getID() == ID.mob.ROSE_GARDEN_PH and os.time() >= rosere then
        DisallowRespawn(ID.mob.ROSE_GARDEN_PH, true)
        mob:setStatus(tpz.status.INVISIBLE)
        DespawnMob(ID.mob.ROSE_GARDEN_PH)
        DisallowRespawn(ID.mob.ROSE_GARDEN, false)
        pos = mob:getPos()
        SpawnMob(ID.mob.ROSE_GARDEN):setPos(pos.x, pos.y, pos.z, pos.rot)
        SetServerVariable("RoseGardenUP", 1)
    end
end

function onMobDeath(mob, player, isKiller)
end

function onMobDespawn(mob)
    if mob:getID() == ID.mob.ROSE_GARDEN_PH then
        SetServerVariable("RosetimeToGrow", 0)
    end
end