-----------------------------------
-- Area: Yuhtunga Jungle
--  Mob: Rose Garden
-----------------------------------
local ID = require("scripts/zones/Yuhtunga_Jungle/IDs")
require("scripts/globals/status")
-----------------------------------

function onMobSpawn(mob)
    local rosere2 = GetServerVariable("RosetimeToGroww")
    local vivianup = GetServerVariable("VivianUP")
    if rosere2 > os.time() then 
        return -- If server restart/crashes, keep first roll
    elseif rosere2 < os.time() and vivianup == 0 then
        SetServerVariable("RosetimeToGroww", os.time() + math.random(60, 61)) -- 10:00:00 to 10:30:00
    end
end

function onMobDisengage(mob)
    SetServerVariable("RosetimeToGroww", os.time() + math.random(60, 61)) -- 10:00:00 to 10:30:00
end

function onMobRoam(mob)
    -- Rose Garden has been left alone for 10.25 hours
    local rosere2 = GetServerVariable("RosetimeToGroww")
    if os.time() >= rosere2 then
        DisallowRespawn(ID.mob.ROSE_GARDEN, true)
        mob:setStatus(tpz.status.INVISIBLE)
        DespawnMob(ID.mob.ROSE_GARDEN)
        DisallowRespawn(ID.mob.VOLUPTUOUS_VILMA, false)
        pos = mob:getPos()
        SpawnMob(ID.mob.VOLUPTUOUS_VILMA):setPos(pos.x, pos.y, pos.z, pos.rot)
        SetServerVariable("RoseGardenUP", 0)
        SetServerVariable("VivianUP", 1)
        SetServerVariable("RosetimeToGroww", 0)
    end
end

function onMobDeath(mob, player, isKiller)
end

function onMobDespawn(mob)
    local rosere2 = GetServerVariable("RosetimeToGrow2")
    if os.time() < rosere2 then    
        DisallowRespawn(ID.mob.ROSE_GARDEN, true)
        DisallowRespawn(ID.mob.ROSE_GARDEN_PH, false)
        GetMobByID(ID.mob.ROSE_GARDEN_PH):setRespawnTime(GetMobRespawnTime(ID.mob.ROSE_GARDEN_PH))        
    end
end
