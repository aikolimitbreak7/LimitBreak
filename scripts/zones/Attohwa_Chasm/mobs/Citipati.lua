-----------------------------------
-- Area: Attohwa Chasm
--  Mob: Citipati
-----------------------------------
require("scripts/globals/hunts")

function onMobRoam(mob)
	me = mob:getID()
	if VanadielHour() > 3 and VanadielHour() < 20 then
		DespawnMob(me)
	end
end

function onMobDeath(mob, player, isKiller)
    tpz.hunts.checkHunt(mob, player, 278)
end
