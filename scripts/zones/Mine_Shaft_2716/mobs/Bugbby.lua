-----------------------------------
-- Area: Mine_Shaft_2716
--   NM: Swipostik
-----------------------------------
mixins = {require("scripts/mixins/job_special")}
require("scripts/globals/status")
-----------------------------------

function onMobSpawn(mob)
    mob:addMod(tpz.mod.DOUBLE_ATTACK, 100)
    mob:addMod(tpz.mod.TRIPLE_ATTACK, 30)   

    tpz.mix.jobSpecial.config(mob, {
        specials =
        {
            {id = tpz.jsa.MIGHTY_STRIKES, cooldown = 480}, -- Cooldown of 8 minutes
        },
    })
end
