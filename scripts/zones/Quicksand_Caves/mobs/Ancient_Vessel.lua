-----------------------------------
-- Area: Quicksand Caves
--  Mob: Ancient Vessel
-- Mithra and the Crystal (Zilart 12) Fight
-----------------------------------
require("scripts/globals/missions")
require("scripts/globals/status")
require("scripts/globals/mobs")
-----------------------------------

function onMobInitialize(mob)
    mob:setMobMod(tpz.mobMod.ADD_EFFECT, 1)
end

function onAdditionalEffect(mob, target, damage)
    return tpz.mob.onAddEffect(mob, target, damage, tpz.mob.ae.STUN, {chance = 25})
end

function onMobSpawn(mob)
    mob:setMod(tpz.mod.SILENCERES, 300)
    mob:setMod(tpz.mod.SLEEPRES, 300)
end

function onMonsterMagicPrepare(mob, spell)
    local roll = math.random()

    if mob:getHPP() > 99 then
        return 249 -- Blaze Spikes
        elseif roll < 0.4 then
            return 186 -- Aeroga III
        elseif roll < 0.8 then
            return 176 -- Firaga III
        elseif roll < 0.82 then
            return 254 -- Blind
        elseif roll < 0.84 then
            return 274 -- Sleepga II
        elseif roll < 0.86 then
            return 231 -- Bio II
        elseif roll < 0.88 then
            return 232 -- Bio III
        elseif roll < 0.90 then
            return 204 -- Flare
        elseif roll < 0.92 then
            return 249 -- Blaze Spikes
        elseif roll < 0.94 then
            return 180 -- Blizzaga III
        elseif roll < 0.96 then
            return 196 -- Thundaga III
        elseif roll < 0.98 then
            return 201 -- Waterga III
    end
end

function onMobDeath(mob, player, isKiller)
    if (player:getCurrentMission(ZILART) == tpz.mission.id.zilart.THE_MITHRA_AND_THE_CRYSTAL and player:getCharVar("ZilartStatus") == 1) then
        player:needToZone(true)
        player:setCharVar("AncientVesselKilled", 1)
    end
end