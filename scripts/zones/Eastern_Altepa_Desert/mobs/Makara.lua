-----------------------------------
-- Area: Eastern Altepa Desert
--  Mob: Makara
-- Note: This should be a fishing only mob, but for some reason roams around the desert
-----------------------------------
require("scripts/globals/status")
-----------------------------------

function onMobSpawn(mob)
    mob:setStatus(tpz.status.INVISIBLE)
end
