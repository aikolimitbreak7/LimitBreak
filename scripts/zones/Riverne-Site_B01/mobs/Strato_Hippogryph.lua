-----------------------------------
-- Area: Riverne - Site B01
--  Mob: Strato Hippogryph
-- Note:
-----------------------------------
local ID = require("scripts/zones/Riverne-Site_B01/IDs")
require("scripts/globals/mobs")
-----------------------------------

function onMobInitialize(mob)
    mob:setMobMod(tpz.mobMod.ADD_EFFECT, 1)
end

function onAdditionalEffect(mob, target, damage)
    local rand = math.random(1,2)
    if rand == 1 then
        return tpz.mob.onAddEffect(mob, target, damage, tpz.mob.ae.WEIGHT)
    end
end

function onMobDeath(mob, player, isKiller)
end