-----------------------------------
-- Area: Boneyard_Gully
--  Mob: Shikaree Y
-- TODO: Should hold TP until all 3 Shikaree have TP for a 3x WS
-- TODO: Chat messages for Starting a SC, Saving TP
-----------------------------------
local ID = require("scripts/zones/Boneyard_Gully/IDs")
-----------------------------------

function onMobFight(mob, target)
    mob:setMod(tpz.mod.REGAIN, 70) -- Took out 70 innate regain from modid causing it to spam WS before the fight started. Increased to 70 in LUA to compensate.
end

function onMobEngage(mob)
end

function onMobInitialize(mob)
	mob:addListener("WEAPONSKILL_STATE_ENTER", "WS_START_MSG", function(mob, wsid)
		if (wsid == 101) or (wsid == 102) or (wsid == 104) then
			mob:showText(mob, ID.text.SHIKAREE_Y_WS2)
		end
	end)
end

function onMobDeath(mob, player, isKiller)
    mob:showText(mob, ID.text.SHIKAREE_Y_DEATH)
end