-----------------------------------
-- Area: Palborough Mines
--   NM: Ni'Ghu Nestfender
-----------------------------------
mixins = {require("scripts/mixins/job_special")}
-----------------------------------
function onMobSpawn(mob)
	mob:setMod(tpz.mod.SILENCERES, 1650)
	mob:setMod(tpz.mod.DEF, 600)
end

function onMonsterMagicPrepare(mob)
    local roll = math.random()

    if mob:getHPP() < 40 then
        if roll < 0.4 then
            return 3 -- Cure III
        elseif roll < 0.8 then
            return 4 -- Cure IV
        elseif roll < 0.9 then
            return 46 -- Protect IV
        else
            return 112 -- Flash
        end
    elseif mob:getHPP() < 98 then
        if roll < 0.35 then
            return 112 -- Flash
        elseif roll < 0.55 then
            return 21 -- Holy
        elseif roll < 0.55 then
            return 29 -- Banish II
        elseif roll < 0.85 then
            return 46 -- Protect 4
        else
            return 3 -- Cure III
        end
    elseif mob:getHPP() > 97 then
        return 21 -- Holy
    end
end

function onMobDeath(mob, player, isKiller)
end

