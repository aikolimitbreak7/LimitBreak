-----------------------------------
-- Area: Jade Sepulcher
--  Mob: Qultada
-- COR LB5
-----------------------------------
local ID = require("scripts/zones/Talacca_Cove/IDs")
require("scripts/globals/status")
require("scripts/globals/mobs")
-----------------------------------

	
function onMobInitialize(mob)

--	mob:addListener("WEAPONSKILL_STATE_ENTER", "WS_START_MSG", function(mob, wsid)
--		if (wsid ~= nil) then
--			mob:showText(mob, ID.text.RAUBAHN_WEAPON_SKILL)
--		end
--	end)
--	mob:addListener("MAGIC_START", "MAGIC_MSG", function(mob, spell, action)
--		
--		local message = math.random(1,2)
--			if message == 1 then
--			mob:showText(mob, ID.text.RAUBAHN_CASTING_ONE)
--			elseif message == 2 then
--			mob:showText(mob, ID.text.RAUBAHN_CASTING_TWO)
--			end
--		
--	end)
--	
end

function onMobSpawn(mob)
    mob:setLocalVar("Percent", 1)
end


function onMobFight(mob, player)
	local hpp = mob:getHPP()
	local percentchat = mob:getLocalVar("Percent")
	
	if percentchat == 1 and hpp <= 70 then
		mob:showText(mob, ID.text.QULTADA_HEAT)
		mob:setLocalVar("Percent", 2)	
	elseif percentchat == 2 and hpp <= 40 then
		mob:showText(mob, ID.text.QULTADA_HEAT)
		mob:setLocalVar("Percent", 3)
	end
	
	if hpp <= 20 then
        mob:getBattlefield():win()
		mob:showText(mob, ID.text.QULTADA_VICTORY)
    end
end

function onMobEngaged(mob, target)
	target:showText(mob, ID.text.QULTADA_ENGAGE)
end

function onMobDeath(mob, player, isKiller)
	player:showText(mob, ID.text.QULTADA_VICTORY)
end
