-----------------------------------
-- Area: Zeruhn Mines (172)
--  Mob: Burrower Worm
-----------------------------------
local ID = require("scripts/zones/Zeruhn_Mines/IDs")
-- mixins = {require("scripts/mixins/families/worm")}
require("scripts/globals/regimes")
require("scripts/globals/mobs")
-----------------------------------

function onMobDeath(mob, player, isKiller)
    tpz.regime.checkRegime(player, mob, 629, 2, tpz.regime.type.GROUNDS)
end
function onMobDespawn(mob)
    tpz.mob.phOnDespawn(mob, ID.mob.GIANT_AMOEBA_PH, 50, 3600) -- 1 hour
end
